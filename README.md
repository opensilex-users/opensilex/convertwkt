# convertWKT

## Introduction

This script is used to convert coordinates from one projection to another, and to WKT points. In order the steps are:
1. Latitude and longitude coordinates conversion from one projection system to the other
2. Creation of two columns named *transformed latitude* and *transformed longitude* with converted coordinates from step 1
3. Conversion of your coordinates into **wgs84** projection and store it in another column named *Longitude_wsg84* and *Latitude_wsg84*
4. Store **WKT points**, based on the calculated coordinates from step 3, in a column named *WKT*

## Installation

### Download from GitLab

Clone the git repository:

```bash
git clone git@forgemia.inra.fr:opensilex-users/opensilex/convertwkt.git
```

### Conda environment

If you don't have conda installed on your computer, follow this [documentation](https://docs.conda.io/projects/conda/en/latest/user-guide/install/index.html#regular-installation).

Go to the project directory and create the *convertWKT* environment using the YAML file coming with the repository:

```bash
conda env create -f env/environment.yaml
```

Activate the environment:

```bash
conda activate convertWKT
```

## How to use the script?

This script is pretty easy to use. First, prepare you input file containing the list of coordinates you want to convert. You can see an example of coordinates file in the `data` folder (columns names don't matter, name them whatever you want).

Then, run the following command:

```bash
python3 convertWKT.py
```

A window will open and tell you to select the file containing your list of coordinates. Then enter initial and destination EPSG. And select your latitue and longitude columns based on their headers.

For example: enter **2154** for **lambert93** as initial EPSG and **4326** for **wgs84** as destination EPSG (it is not interesting here as initial coordinates are always converted to wgs84).

To finish, select a file (or enter the name of a new file) where converted coordinates will be saved into.
