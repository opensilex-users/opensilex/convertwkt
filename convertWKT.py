import tkinter as tk
from tkinter import filedialog, messagebox
import pandas as pd
from pyproj import Transformer
from shapely.geometry import Point
from shapely import wkt

class CSVColumnSelector(tk.Tk):
    def __init__(self):
        super().__init__()
        self.title("CSV Column Selector")

        self.file_path = ""
        self.latitude_column = ""
        self.longitude_column = ""
        self.columns = []

        self.create_widgets()

    def create_widgets(self):
        self.file_button = tk.Button(self, text="Open CSV", command=self.open_csv)
        self.file_button.pack(pady=10)

    def open_csv(self):
        """
        The `open_csv` function opens a CSV file, reads its contents into a pandas DataFrame, and displays
        the column selection interface.
        :return: If the `self.file_path` is empty or not selected by the user, the function will return
        without performing any further actions.
        """
        self.file_path = filedialog.askopenfilename(filetypes=[("CSV files", "*.csv")])
        if not self.file_path:
            return

        self.df = pd.read_csv(self.file_path)
        self.columns = self.df.columns.tolist()

        self.show_column_selection()

    def show_column_selection(self):
        """
        The function `show_column_selection` creates a window for selecting latitude and longitude columns,
        as well as initial and destination EPSG values, with a submit button to get the selected values.
        """
        #window config title
        self.column_selection_window = tk.Toplevel(self)
        self.column_selection_window.title("Select Columns")

        #Latitude widget
        tk.Label(self.column_selection_window, text="Select Latitude Column:").pack(pady=5)
        self.lat_column_var = tk.StringVar(self.column_selection_window)
        self.lat_column_var.set(self.columns[0])
        self.lat_column_menu = tk.OptionMenu(self.column_selection_window, self.lat_column_var, *self.columns)
        self.lat_column_menu.pack(pady=5)

        #Longitude widget
        tk.Label(self.column_selection_window, text="Select Longitude Column:").pack(pady=5)
        self.lon_column_var = tk.StringVar(self.column_selection_window)
        self.lon_column_var.set(self.columns[0])
        self.lon_column_menu = tk.OptionMenu(self.column_selection_window, self.lon_column_var, *self.columns)
        self.lon_column_menu.pack(pady=5)

        #Initial epsg entry
        tk.Label(self.column_selection_window, text="Initial EPSG:").pack(pady=10)
        self.origin_epsg_entry = tk.Entry(self.column_selection_window)
        self.origin_epsg_entry.pack(pady=10)

        #Destination epsg entry
        tk.Label(self.column_selection_window, text="Destination EPSG:").pack(pady=10)
        self.dest_epsg_entry = tk.Entry(self.column_selection_window)
        self.dest_epsg_entry.pack(pady=10)

        #Submit button
        self.submit_button = tk.Button(self.column_selection_window, text="Submit", command=self.get_values)
        self.submit_button.pack(pady=20)

    def get_values(self):
        #Get inputs from window
        self.latitude_column = self.lat_column_var.get()
        self.longitude_column = self.lon_column_var.get()
        self.origin_epsg = self.origin_epsg_entry.get()
        self.dest_epsg = self.dest_epsg_entry.get()
        self.column_selection_window.destroy()

        self.transform_coordinates()

    def transform_coordinates(self):
        """
        The `transform_coordinates` function transforms coordinates from one EPSG projection to another chosen by the user,
        converts coordinates to WSG84 format and into WKT points, and saves the transformed data as a CSV file.
        """
        #Convert epsg depending on user choices
        transformer = Transformer.from_crs(self.origin_epsg,self.dest_epsg, always_xy=True)
        #Add transformed coordinates to our df.
        self.df[['Transformed_Longitude', 'Transformed_Latitude']] = self.df.apply(
            lambda row: pd.Series(transformer.transform(row[self.longitude_column], row[self.latitude_column])),
            axis=1
        )
        #Convert to wsg84 and add to our df
        transformer = Transformer.from_crs(self.origin_epsg,"4326", always_xy=True)
        self.df[['Longitude_wgs84', 'Latitude_wgs84']] = self.df.apply(
            lambda row: pd.Series(transformer.transform(row[self.longitude_column], row[self.latitude_column])),
            axis=1
        )
        #Create wkt coordinates add them to our df
        self.df['WKT'] = self.df.apply(
            lambda row: wkt.dumps(Point(row['Longitude_wgs84'], row['Latitude_wgs84'])),
            axis=1
        )
        #Save the df to csv
        save_path = filedialog.asksaveasfilename(defaultextension=".csv", filetypes=[("CSV files", "*.csv")])
        #Check success
        if save_path:
            self.df.to_csv(save_path, index=False)
            messagebox.showinfo("Success", f"Transformed CSV saved as {save_path}")


#Run the window and keep it open
if __name__ == "__main__":
    app = CSVColumnSelector()
    app.mainloop()
